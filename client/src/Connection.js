import { useWallet, UseWalletProvider } from 'use-wallet';
import React from 'react';

function Connection() {
    const wallet = useWallet();

    const connectWallet = async (e) => {
        e.preventDefault();
        await wallet.connect();
    };

    const disconnectWallet = async (e) => {
        e.preventDefault();
        await wallet.reset();
    };

    return (
        <>
            <div>
                <button onClick={connectWallet}>Connect Wallet</button>
            </div>
            <div>
                <button onClick={disconnectWallet}>DISCONNECT Wallet</button>
            </div>
        </>
    );
}

// Wrap everything in <UseWalletProvider />
export default () => (
    <UseWalletProvider
        chainId={1337} //local chain id, rankbey:4, mainet:1
        connectors={{
            // This is how connectors get configured
            provided: { provider: window.cleanEthereum },
        }}
    >
        <Connection />
    </UseWalletProvider>
);
