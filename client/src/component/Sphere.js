import React, { Suspense } from 'react';
import { Sphere, MeshDistortMaterial } from '@react-three/drei';

export const SphereGeo = () => {
    return (
        <>
            <Sphere visible args={[1, 100, 200]} scale={2}>
                <MeshDistortMaterial color="#8352FD" attach="material" distort={0.4} speed={4} roughness={0.4} />
            </Sphere>
        </>
    );
};
