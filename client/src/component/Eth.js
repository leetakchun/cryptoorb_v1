/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
author: Alexandr Akimov (https://sketchfab.com/akimovcg)
license: CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
source: https://sketchfab.com/3d-models/ethereum-3d-logo-de0c154b6f1c46639864a01360dd6aa0
title: Ethereum 3D logo
*/

import React, { useRef, useEffect } from 'react';
import { useGLTF, softShadows, MeshWobbleMaterial, useProgress } from '@react-three/drei';
import { useSpring, a } from '@react-spring/three';
import { useFrame } from '@react-three/fiber';
import { DefaultLoadingManager } from 'three';

export default function Model({ ...props }) {
    const group = useRef();
    const { nodes, materials } = useGLTF('/eth.gltf');
    useFrame(() => (group.current.rotation.y -= 0.02));
    //useFrame需要另在Canvas之內，即要另外開一個component做mesh
    return (
        <group ref={group} {...props} dispose={null}>
            <group rotation={[-Math.PI / 2, 0, 0]} scale={0.01} position={[0, 1.3, 0]}>
                <group scale={0.5}>
                    <mesh castShadow geometry={nodes.mesh_0.geometry} material={materials['default']} />
                </group>
            </group>
        </group>
    );
}

useGLTF.preload('/eth.gltf');
