import React, { Suspense } from 'react';
import { useLoader } from '@react-three/fiber';
import { TextureLoader } from 'three';
import texture from './gold_texture.png';

export const BoxGeo = () => {
    const colorMap = useLoader(TextureLoader, texture);
    return (
        <>
            <mesh rotation={[90, 0, 20]}>
                <boxGeometry args={[2, 3, 4]} />
                {/* <meshLambertMaterial map={colorMap} /> */}
                <meshNormalMaterial attach="material" />
            </mesh>
        </>
    );
};
