import React, { Suspense, useRef, useState, useEffect } from 'react';
import { OrbitControls, useCursor } from '@react-three/drei';
import { Canvas, useFrame } from '@react-three/fiber';
import { softShadows, MeshWobbleMaterial, Loader, Html } from '@react-three/drei';
import { useSpring, a } from '@react-spring/three';
import * as THREE from 'three';
import Eth from './Eth';

softShadows();
//要係directionalLight設定shadow-mapSize-width等等

// { position } == props.position
// args={[width, height, depth]}
// position={[x,y,z]}
// fx is the callback function
// castShadow for 1.Canvas, 2.mesh, 3.light
const SpinningMesh = ({ position, args, velocity, fx }) => {
    const [hovered, setHovered] = useState(false);
    const [active, setActive] = useState(0);

    //change cursor be pointer which hover mesh
    useCursor(hovered);
    // useEffect(() => {
    //     document.body.style.cursor = hovered ? 'pointer' : 'auto';
    // }, [hovered]);

    //animation on click
    const { spring } = useSpring({
        spring: active,
        config: { mass: 5, tension: 400, friction: 50, precision: 0.0001 },
    });
    // interpolate values from useSpring
    const scale = spring.to([0, 1], [1, 2.2]);
    const rotation = spring.to([0, 1], [0, Math.PI]);
    const color = spring.to([0, 1], ['#6246ea', '#e45858']);

    const mesh = useRef(null); //get DOM information by <ref>.current
    let _velocity = parseFloat(velocity);
    useFrame(({ clock }) => {
        let x = Math.sin(clock.getElapsedTime()) / 50 + _velocity;
        mesh.current.rotation.x = mesh.current.rotation.y += x;
    });
    //useFrame需要另在Canvas之內，即要另外開一個component做mesh
    //a.mesh 代表 animated

    return (
        <a.mesh
            castShadow
            ref={mesh}
            scale={1}
            position={position}
            onClick={(e) => {
                setActive(Number(!active)); //need to use Number to set active or close
            }}
            onPointerOver={(e) => {
                setHovered(true);
                // fx();
            }}
            onPointerOut={(e) => {
                setHovered(false);
                // fx();
            }}
            scale-x={scale}
            scale-y={scale}
            // rotation-y={rotation}
        >
            <boxBufferGeometry attach="geometry" args={args} />
            {/* MeshWobbleMaterial係可令object變形 */}
            <a.meshStandardMaterial roughness={0.5} attach="material" color={color} />
            {/* <MeshWobbleMaterial attach="material" color={color} speed={1} factor={0.4} /> */}
        </a.mesh>
    );
};

export const AnimateGeometry = () => {
    return (
        <>
            {/* Canvas shadows dont's use shadowMap */}
            <Canvas shadows colorManagement camera={{ position: [-5, 4, 10], fov: 44, zoom: 1 }}>
                {/* <primitive object={new THREE.AxesHelper(10)} /> */}
                {/* <OrbitControls enableZoom={false} /> */}
                <ambientLight intensity={0.5} />
                <directionalLight
                    castShadow
                    position={[0, 10, 0]}
                    intensity={1.1}
                    shadow-mapSize-width={1024}
                    shadow-mapSize-height={1024}
                    shadow-camera-far={50}
                    shadow-camera-left={-10}
                    shadow-camera-right={10}
                    shadow-camera-top={10}
                    shadow-camera-bottom={-10}
                />
                <pointLight position={[-10, 0, -20]} intensity={0.5} />
                <pointLight position={[0, -10, 3]} intensity={1.5} />
                <group>
                    <mesh receiveShadow={true} rotation={[-Math.PI / 2, 0, 0]} position={[0, -4, 0]}>
                        <planeBufferGeometry attach="geometry" args={[100, 100]} />
                        {/* shardow 反映 */}
                        <shadowMaterial attach="material" opacity={0.2} />
                        {/* plan定位用 */}
                        {/* <meshStandardMaterial attach="material" color={'yellow'} /> */}
                    </mesh>
                </group>
                <Suspense
                    fallback={
                        <Html>
                            <Loader />
                        </Html>
                    }
                >
                    <Eth />
                </Suspense>
                <SpinningMesh
                    position={[-2, 1, -5]}
                    // color="pink"
                    velocity="0.03"
                    fx={() => {
                        console.log('super girl ~');
                    }}
                />
                <SpinningMesh
                    position={[5, 1, -2]}
                    // color="pink"
                    velocity="0.05"
                    fx={() => {
                        console.log('i am loving it');
                    }}
                />
                <SpinningMesh
                    position={[6.2, 1.2, 2]}
                    // color="pink"
                    velocity="0.066"
                    fx={() => {
                        console.log('i am loving it');
                    }}
                />
                <SpinningMesh
                    position={[9.2, 1.2, 3]}
                    // color="pink"
                    velocity="0.02"
                    fx={() => {
                        console.log('i am loving it');
                    }}
                />
                <SpinningMesh
                    position={[-5, 1.1, -3]}
                    args={[1.1, 1.4, 1]}
                    // color="pink"
                    velocity="0.04"
                    fx={() => {
                        console.log('i am loving it');
                    }}
                />
                <SpinningMesh
                    position={[-8, 1.1, -4]}
                    args={[0.8, 1.4, 1]}
                    // color="pink"
                    velocity="0.06"
                    fx={() => {
                        console.log('i am loving it');
                    }}
                />
            </Canvas>
        </>
    );
};
