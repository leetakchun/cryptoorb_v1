import React, { useRef, useEffect, useState } from 'react';
import _ from 'lodash';

export const Mint_NFT = ({ keyInfo, name, imageUrl, animation_url, description, attributes, contractAddress }) => {
    return (
        <>
            <div key={keyInfo} className="card mb-5 text-white bg-dark" style={{ borderColor: '#262626', width: '18rem' }}>
                {/* <img className="card-img-top" src={imageUrl} alt="Crypto ORB NFT" /> */}
                <iframe src={animation_url} style={{ minWidth: '100%', height: '200px' }}></iframe>
                <div className="card-body">
                    <h5 className="card-title text-center">{name}</h5>
                    <p className="card-text">{description}</p>
                    <ul style={{ fontSize: '14px', paddingLeft: '10px' }}>
                        {attributes.map((el) => {
                            return (
                                <li>
                                    {el.trait_type} : {el.value}
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={() => window.open(`https://rinkeby.etherscan.io/address/${contractAddress}`, '_blank')}
                >
                    etherscan (Rinkeby testnet)
                </button>
            </div>
        </>
    );
};
