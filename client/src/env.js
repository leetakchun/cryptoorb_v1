import dotenv from 'dotenv';
dotenv.config();
// React的.env variable必定要加 "REACT_APP_" 的prefix，否則undefined

export let env = {
    //server
    REACT_APP_API_SERVER: process.env.REACT_APP_API_SERVER,
};
