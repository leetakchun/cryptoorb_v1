import React, { Component, useState, useEffect, Suspense } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect, Route, Switch, useParams, Link } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// import CryptoCoders from '../contracts/CryptoCoders.json';
import Web3 from 'web3';
import { env } from '../env';
import _ from 'lodash';
import bgImage from '../Orb_background.jpg';
import { AiFillPlusSquare } from 'react-icons/ai';
import { AiFillMinusSquare } from 'react-icons/ai';
import { getRandomInt } from '../util/helper';

import { Mint_NFT } from '../component/Mint_NFT';
import { connectThunk } from '../redux/web3/thunks';
import { updateAccount } from '../redux/web3/thunks';
import { connectFailed } from '../redux/web3/action';
import { fetchDataFailed } from '../redux/blockChainData/action';
import { blockChainFetchDataThunk } from '../redux/blockChainData/thunks';
// import Web3Modal from 'web3modal';
// import WalletConnectProvider from '@walletconnect/web3-provider';
// import WalletLink from 'walletlink';

const HomePage = () => {
    //////// useState useSelector/////////
    const dispatch = useDispatch(); //dispatch is only way to trigger store's state change
    const blockchain = useSelector((state) => state.blockchain);
    const data = useSelector((state) => state.data);
    const [contract, setContract] = useState(null);
    const [feedback, setFeedback] = useState("MayBe it's your lucky day!");
    const [metaMaskMsg, setMetaMaskMsg] = useState('');
    const [claimingNft, setClaimingNft] = useState(false);
    const [mintValue, setMintValue] = useState(0.1);
    const [mintAmount, setMintAmount] = useState(1);
    /////////////////////

    //1. AUTO CHECK the web3 is currently connected or not When LOADING the page
    useEffect(async () => {
        if (window.ethereum !== 'undefined') {
            let _web3 = new Web3(window.ethereum);
            // let _connected = window.ethereum.isConnected();
            let _accounts = await _web3.eth.getAccounts();
            // console.log({ _accounts });
            if (_accounts.length > 0) {
                dispatch(connectThunk()); // update blockchain.account state then 2.WILL RUN
                // console.log('connected ~~~ ');
            } else {
                dispatch(connectFailed('not yet connected'));
                // console.log('not connected ....');
            }
        } else {
            setMetaMaskMsg('Please install metaMastMask for ethereum service ~');
        }
    }, []);

    //2. KEEP MONITORING the CHANGE of blockchain.account or contract
    useEffect(() => {
        if (blockchain.account !== '' && blockchain.smartContract !== null) {
            dispatch(blockChainFetchDataThunk(blockchain.account));
        }
    }, [blockchain.smartContract, blockchain.account, dispatch]);

    //3. Check ethereum EVENT (accountsChanged, chainChanged) 因為disconnected無效，要用accountsChanged去check
    useEffect(() => {
        // Check if the metamask wallet changed or disconnected
        window.ethereum.on('accountsChanged', (accounts) => {
            // console.log('accounts @accountsChanged > ', accounts);
            if (accounts.length > 0) {
                dispatch(updateAccount(accounts[0])); //update store
            } else {
                //account disconnected
                dispatch(connectFailed('user disconnected'));
                dispatch(fetchDataFailed('user disconnected'));
            }
        });
        window.ethereum.on('chainChanged', (chainId) => {
            let _chainId = parseInt(chainId, 16); //Hex to Oct
            window.location.reload();
        });
    }, []);

    const mintNft = async (_amount) => {
        let _web3 = new Web3(window.ethereum);
        //prepare the TokenURI information
        let randomInt = getRandomInt(1, 7);
        const res = await fetch(`${env.REACT_APP_API_SERVER}/js/treasureBall_${randomInt}.json`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        });
        const backendJson = await res.json();
        let Color = _.find(backendJson.attributes, (el) => {
            return el.trait_type == 'Color';
        });
        let Material = _.find(backendJson.attributes, (el) => {
            return el.trait_type == 'Material';
        });
        let Level = _.find(backendJson.attributes, (el) => {
            return el.trait_type == 'Level';
        });

        let payCost = _amount * mintValue;

        // console.log("blockchain.account >> ", blockchain.account); //backendJson.attributes is array

        setClaimingNft(true); //changing STATE for loading
        await blockchain.smartContract.methods
            .mint(
                blockchain.account,
                _amount,
                backendJson.name,
                Material.value,
                Color.value,
                backendJson.animation_url,
                backendJson.image,
                backendJson.description,
                Level.value,
            )
            .send({
                from: blockchain.account,
                value: _web3.utils.toWei(payCost.toString(), 'ether'),
            })
            .on('receipt', (receipt) => {
                console.log(receipt);
                dispatch(blockChainFetchDataThunk(blockchain.account)); //update redux blockchain data
                setClaimingNft(false); //changing STATE for loading
            })
            .on('error', (error) => {
                console.log('error >> ', error);
                setClaimingNft(false); //changing STATE for loading
            });
    };

    const withdrawEth = async () => {
        setClaimingNft(true); //changing STATE for loading
        await blockchain.smartContract.methods
            .withdraw()
            .send({
                from: blockchain.account,
            })
            .on('receipt', (receipt) => {
                console.log(receipt);
                setClaimingNft(false); //changing STATE for loading
            })
            .on('error', (error) => {
                console.log('error >> ', error);
                setClaimingNft(false); //changing STATE for loading
            });
    };

    const addAmount = () => {
        setMintAmount((e) => e + 1);
    };

    const reduceAmount = () => {
        if (mintAmount > 1) {
            setMintAmount((e) => e - 1);
        }
    };

    return (
        <>
            <div
                style={{
                    backgroundImage: `url(${bgImage})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'contain',
                    backgroundPosition: 'center',
                    backgroundColor: 'black',
                }}
            >
                <div className="container-fluid min-vh-100 ">
                    <div className="row">
                        {/* 用flex-column 便可做到似好多row, 用align-items-center便全可置中 */}
                        <div className="col d-flex flex-column align-items-center">
                            <div className="mt-3 mb-2">
                                <iframe
                                    src={`${env.REACT_APP_API_SERVER}/treasureBall_Main.html`}
                                    style={{ minWidth: '100%', height: '200px' }}
                                ></iframe>
                            </div>
                            <h1 className="text-light fw-bold">Crypto ORB</h1>
                            <div className="col-5 text-center">
                                <p className="text-center text-light lead">
                                    Jerry, the creator of the Crypto ORB, on-chain Dynamic NFT with 3D Orb. The metadata is on-chain. The
                                    Crypto ORB NFT is generated randomly when you mint. You can enjoy the Orb when you drag it. Good luck
                                    and hope you mint your lovely Orb!
                                </p>
                                <p className="text-center text-light">
                                    | Testing phase, please connect your metamask with Rinkeby network |
                                </p>
                                <div className="col d-flex flex-column justify-content-center align-items-center mb-5">
                                    {metaMaskMsg != '' ? (
                                        <>
                                            <div className="mt-4 mt-4">
                                                <button
                                                    className=" col-4 btn btn-light"
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        window.open('https://metamask.io/', '_blank');
                                                    }}
                                                >
                                                    {metaMaskMsg}
                                                </button>
                                            </div>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                    {blockchain.contractCreator ? (
                                        <>
                                            <button
                                                disabled={claimingNft}
                                                className=" col-4 mb-3 btn btn-primary"
                                                onClick={(e) => {
                                                    e.preventDefault();
                                                    withdrawEth();
                                                }}
                                            >
                                                Withdraw
                                            </button>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                    {!blockchain.connected ? (
                                        <button
                                            className=" col-4 btn btn-light"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                dispatch(connectThunk());
                                                //Connect controlled by blockchain.connected
                                            }}
                                        >
                                            CONNECT
                                        </button>
                                    ) : (
                                        <>
                                            <button
                                                disabled={claimingNft}
                                                className=" col-4 btn btn-light"
                                                onClick={() => {
                                                    mintNft(mintAmount);
                                                }}
                                            >
                                                {claimingNft ? 'Loading ...' : 'Mint !'}
                                            </button>
                                        </>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="offset-2 col-8 d-flex flex-wrap justify-content-around mt-4">
                            {data.ownerNftTokenURI.map((el, key) => {
                                return (
                                    <Mint_NFT
                                        keyInfo={key}
                                        name={el.name}
                                        imageUrl={el.image}
                                        animation_url={el.animation_url}
                                        description={el.description}
                                        attributes={el.attributes}
                                        contractAddress={data.contractAddress}
                                    />
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default HomePage;
