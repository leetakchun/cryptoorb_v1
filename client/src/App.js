import React, { Component, useState, useEffect, Suspense } from 'react';
import { Redirect, Route, Switch, useParams, Link } from 'react-router-dom';
import CryptoCoders from './contracts/CryptoCoders.json';
import getWeb3 from './getWeb3';
import NftPage from './pages/NftPage';
import HomePage from './pages/HomePage';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useSelector, useDispatch } from 'react-redux';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Button,
} from 'reactstrap';
import './App.css';

const App = () => {
    const getWindowDimensions = () => {
        const { innerWidth: width, innerHeight: height } = window;
        return {
            width,
            height,
        };
    };

    const [isOpen, setIsOpen] = useState(false);
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    /////////////// Redux  ///////////////
    const dispatch = useDispatch();
    const blockchain = useSelector((state) => state.blockchain);

    const toggle = () => {
        //Check the width of the screen
        if (windowDimensions.width > 768) {
            setIsOpen(() => false);
        } else {
            setIsOpen(!isOpen);
        }
    };

    const handleResize = () => {
        setWindowDimensions(getWindowDimensions);
    };

    //nav-bar
    useEffect(() => {
        window.addEventListener('resize', handleResize);
        return () => {
            return window.addEventListener('resize', handleResize);
        };
    }, []);

    return (
        <>
            <Navbar color="dark" light expand="md">
                <NavbarBrand className="navBrand text-white">Crypto ORB </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem className="mr-2">
                            <NavLink href="/" onClick={toggle} style={{ cursor: 'pointer', color: '#EEEEEE' }}>
                                HOMEPAGE
                            </NavLink>
                        </NavItem>
                        {/* <NavItem className="mr-2">
                            <NavLink href="/nft" onClick={toggle} style={{ cursor: 'pointer', color: '#EEEEEE' }}>
                                NFT
                            </NavLink>
                        </NavItem> */}
                        <NavItem className="mr-2">
                            <NavLink onClick={toggle} style={{ cursor: 'auto', color: '#c5d3eb' }}>
                                {blockchain.account}
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Switch>
                <Route path="/" exact={true} component={HomePage} />
                <Route exact path="/nft" component={NftPage} />
            </Switch>
        </>
    );
};

export default App;
