export function loginAction(payload) {
    return {
        type: '@Auth/login',
        payload,
    };
}

export function logoutAction() {
    return {
        type: '@Auth/logout',
    };
}

export function failedAction(msg) {
    return {
        type: '@Auth/failed',
        msg,
    };
}
