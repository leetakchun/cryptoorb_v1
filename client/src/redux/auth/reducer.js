const initialState = {
    status: 'loading',
    role: 'unknown',
    user: null,
    error: '',
    token: localStorage.getItem('token'),
    refresh_token: localStorage.getItem('refresh_token'),
};

//action function 由app.js 先要import入去做

export let authReducers = (state = initialState, action) => {
    switch (action.type) {
        case '@Auth/failed':
            return {
                ...state,
                status: 'unknown',
                error: action.msg,
                role: 'guest',
                token: null,
                refresh_token: null,
            };
        case '@Auth/login':
            return {
                ...state,
                status: 'ready',
                user: action.payload.user,
                role: action.payload.role,
                token: action.payload.token,
                refresh_token: action.payload.refresh_token,
            };
        case '@Auth/logout':
            return {
                ...state,
                status: 'unknown',
                user: null,
                role: 'guest',
                token: null,
                refresh_token: null,
            };
        default:
            return state;
    }
};
