const initialState = {
    loading: false,
    connected: false,
    account: null,
    smartContract: null,
    contractCreator: false,
    errorMsg: '',
};

export let blockchainReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'CONNECTION_REQUEST':
            return {
                ...initialState,
                loading: true,
            };
        case 'CONNECTION_SUCCESS':
            return {
                ...state,
                loading: false,
                connected: true,
                account: action.payload.account,
                smartContract: action.payload.smartContract,
                contractCreator: action.payload.contractCreator,
                // web3: action.payload.web3,
            };
        case 'CONNECTION_FAILED':
            return {
                ...initialState,
                loading: false,
                connected: false,
                errorMsg: action.payload,
            };
        case 'UPDATE_ACCOUNT':
            return {
                ...state,
                account: action.payload.account,
                contractCreator: action.payload.contractCreator,
            };
        default:
            return state;
    }
};
