// constants
import Web3 from 'web3';
import CryptoOrb from '../../contracts/CryptoOrb.json';
import { connectRequest } from './action';
import { connectSuccess } from './action';
import { updateAccountRequest } from './action';
import { connectFailed } from './action';
import { store } from '../store';
//BlockChainData action
import { blockChainFetchDataThunk } from '../blockChainData/thunks';

export const connectThunk = () => {
    return async (dispatch) => {
        //thunk化的action (closure)
        dispatch(connectRequest()); //set initial state of reducer, update store
        if (window.ethereum) {
            let web3 = new Web3(window.ethereum);
            try {
                const accounts = await window.ethereum.request({
                    method: 'eth_requestAccounts',
                });
                const networkId = await window.ethereum.request({
                    method: 'net_version',
                });
                const NetworkData = await CryptoOrb.networks[networkId];
                if (NetworkData) {
                    const SmartContractObj = new web3.eth.Contract(CryptoOrb.abi, NetworkData.address);
                    // const CoinContractObj = new web3.eth.Contract(CoinContract.abi, NetworkData.address); ==> for defi
                    // check current account is the contract creator or not
                    const contractOwner = await SmartContractObj.methods.owner().call(); //checksum address
                    let currentAddress = accounts[0]; // not checksum address
                    currentAddress = web3.utils.toChecksumAddress(currentAddress.toLowerCase());
                    let contractCreatorCheck = false;
                    // console.log({ currentAddress, contractOwner });
                    if (currentAddress == contractOwner) {
                        contractCreatorCheck = true;
                    }
                    dispatch(
                        connectSuccess({
                            //update store
                            account: accounts[0],
                            smartContract: SmartContractObj, //it is ok
                            contractCreator: contractCreatorCheck,
                            // web3: web3, //may cause not "sync block" problem
                        }),
                    );
                } else {
                    dispatch(connectFailed('Change network to Polygon.'));
                }
            } catch (err) {
                dispatch(connectFailed('Something went wrong.'));
            }
        } else {
            dispatch(connectFailed('Install Metamask.'));
        }
    };
};

export const updateAccount = (account) => {
    return async (dispatch) => {
        let web3 = new Web3(window.ethereum);
        let contract = await store.getState().blockchain.smartContract;
        const contractOwner = await contract.methods.owner().call(); //checksum address

        let currentAddress = web3.utils.toChecksumAddress(account.toLowerCase()); //lowerCase convert to checksum address
        let contractCreatorCheck = false;
        // console.log({ currentAddress, contractOwner });
        if (currentAddress == contractOwner) {
            contractCreatorCheck = true;
        }

        dispatch(updateAccountRequest({ account: account, contractCreator: contractCreatorCheck }));
        dispatch(blockChainFetchDataThunk(account));
    };
};
