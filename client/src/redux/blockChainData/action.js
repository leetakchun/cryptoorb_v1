export const fetchDataRequest = () => {
    return {
        type: 'CHECK_DATA_REQUEST',
    };
};

export const fetchDataSuccess = (payload) => {
    return {
        type: 'CHECK_DATA_SUCCESS',
        payload: payload,
    };
};

export const fetchDataFailed = (payload) => {
    return {
        type: 'CHECK_DATA_FAILED',
        payload: payload,
    };
};
