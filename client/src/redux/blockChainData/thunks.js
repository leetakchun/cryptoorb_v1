import Web3 from 'web3';
import { store } from '../store';
import CryptoOrb from '../../contracts/CryptoOrb.json';
import { fetchDataRequest } from './action';
import { fetchDataSuccess } from './action';
import { fetchDataFailed } from './action';

//decode Base64 and convert to JSON format
function UnicodeDecodeB64(str) {
    try {
        //grap base64 string
        let mBase64Str = str.split('data:application/json;base64,');
        let result = window.atob(mBase64Str[1]);
        return JSON.parse(result);
    } catch (error) {
        console.log('decode Base64 error');
    }
}

export const blockChainFetchDataThunk = () => {
    return async (dispatch) => {
        dispatch(fetchDataRequest()); //set state loading
        let web3 = new Web3(window.ethereum);
        try {
            //contract call method and update the NFT state of the account
            let contract = await store.getState().blockchain.smartContract;
            let accountAddress = await store.getState().blockchain.account;
            let name = await contract.methods.name().call();
            let ownerNftIds = await contract.methods.walletOfOwner(accountAddress).call(); //Array
            let ownerNftTokenURI = [];
            //Need Promise.all to wait the long minting process after the record is loading
            await Promise.all(
                ownerNftIds.map(async (id) => {
                    //All web3 need toString(), and call() for 'get', send() for 'update state'
                    let rawTokenURI = await contract.methods.tokenURI(id.toString()).call();
                    let decodeTokenURI = UnicodeDecodeB64(rawTokenURI);
                    decodeTokenURI['nftTokenId'] = id;
                    ownerNftTokenURI.push(decodeTokenURI);
                }),
            );
            // console.log({ contract, accountAddress, name, ownerNftIds });

            //get contract address
            const networkId = await web3.eth.net.getId();
            const networkData = CryptoOrb.networks[networkId];
            let address = networkData.address;
            // let _accountAddress = await web3.eth.getAccounts(); for ref

            //setParam and dispatch update store
            let payload = { name, address, accountAddress, ownerNftTokenURI };
            dispatch(fetchDataSuccess(payload));
        } catch (err) {
            console.log(err);
            dispatch(fetchDataFailed('Could not load data from contract.'));
        }
    };
};
