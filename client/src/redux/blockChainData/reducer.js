const initialState = {
    loading: false,
    name: '',
    accountAddress: '',
    error: false,
    errorMsg: '',
    contractAddress: '',
    ownerNftTokenURI: [],
};

export let dataReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'CHECK_DATA_REQUEST':
            return {
                ...initialState,
                loading: true,
            };
        case 'CHECK_DATA_SUCCESS':
            return {
                ...initialState,
                loading: false,
                name: action.payload.name,
                contractAddress: action.payload.address,
                accountAddress: action.payload.accountAddress,
                ownerNftTokenURI: action.payload.ownerNftTokenURI,
            };
        case 'CHECK_DATA_FAILED':
            return {
                ...initialState,
                loading: false,
                error: true,
                errorMsg: action.payload,
            };
        default:
            return state;
    }
};
