import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import { RouterState, connectRouter, routerMiddleware, CallHistoryMethodAction } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk';
// action, reducer
// import { AuthAction } from './auth/action';
import { authReducers } from './auth/reducer';
import { blockchainReducers } from './web3/reducer';
import { dataReducers } from './blockChainData/reducer';

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history), //must included
    auth: authReducers,
    blockchain: blockchainReducers,
    data: dataReducers,
});

//for redux tool checking
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootEnhancer = composeEnhancers(applyMiddleware(logger), applyMiddleware(routerMiddleware(history)), applyMiddleware(thunk));

export let store = createStore(rootReducer, rootEnhancer);
