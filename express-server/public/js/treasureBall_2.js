import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { EffectComposer } from 'EffectComposer';
import { UnrealBloomPass } from 'UnrealBloomPass';
import { RenderPass } from 'RenderPass';
import { GLTFLoader } from 'GLTFLoader';
import { GlitchPass } from 'GlitchPass';
import { FlakesTexture } from 'FlakesTexture';
import { RGBELoader } from 'RGBELoader';

//setup global variable
let camera, scene, renderScene, renderer, container, controls, _EffectComposer, _UnrealBloomPass, pointLight;
let geometry, material, mesh, cube, w, h, ratio, light1, light2, plane, clock, mixer;
let _color = ['#cb36f5', '#f53689', '#f5ac36', '#f56936', '#169c42', '#16879c', '#16879c'];

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(50, ratio, 1, 1000);
    camera.position.set(0, 0, 500);

    //render
    renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
    renderer.setSize(w, h); //may need w,h,false
    renderer.setPixelRatio(window.devicePixelRatio);
    // renderer.setClearColor(0x111b44); //background color
    container = document.getElementById('three_model'); //depends on div id
    container.appendChild(renderer.domElement);

    //encoding
    renderer.outputEncoding = THREE.sRGBEncoding;
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1.25;

    //EffectComposer > UnrealBloomPass
    _EffectComposer = new EffectComposer(renderer); //need webglrender for arg.
    _UnrealBloomPass = new UnrealBloomPass(new THREE.Vector2(w, h)); //constructor(resolution, strength, radius, threshold)
    _UnrealBloomPass.exposure = 0.3;
    _UnrealBloomPass.strength = 0.41;
    _UnrealBloomPass.radius = 1; //擴散到幾遠
    _UnrealBloomPass.threshold = 0.9; //演示程度
    let _Glitchpass = new GlitchPass(1);

    _EffectComposer.setSize(w, h);
    _EffectComposer.addPass(new RenderPass(scene, camera));
    _EffectComposer.addPass(_UnrealBloomPass);
    // _EffectComposer.addPass(_Glitchpass);
    // _EffectComposer.addPass(new UnrealBloomPass({ x: w, y: h }, 2.0, 0.0, 0.75));

    //clock
    clock = new THREE.Clock();

    // SCENE
    scene.background = new THREE.Color('#000000');

    //Controls
    controls = new OrbitControls(camera, renderer.domElement); //centre of the div
    controls.enableZoom = false;
    // controls.target = new THREE.Vector3(0, 0, -40); //set the target

    controls.autoRotate = true;
    controls.autoRotateSpeed = 10;
    controls.enableDamping = true; //有用尾感

    // INIT HEMISPHERE LIGHT
    pointLight = new THREE.PointLight(0xff6666, 1);
    pointLight.position.set(200, 200, 200);
    scene.add(pointLight);
    // scene.add(new THREE.AmbientLight(0xffffff, 0.5));

    //LoadTextureFile
    let envmaploader = new THREE.PMREMGenerator(renderer);

    new RGBELoader().setPath('../texture/').load('photo_studio_loft_hall_4k_720.hdr', (hdrmap) => {
        //Texture
        let envmap = envmaploader.fromCubemap(hdrmap);
        let texture = new THREE.CanvasTexture(new FlakesTexture());
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.x = 10;
        texture.repeat.y = 6;

        let ballMaterial = {
            clearcoat: 1.0,
            clearcoatRoughness: 0.1,
            metalness: 0.9,
            roughness: 0.5,
            color: _color[1],
            normalMap: texture,
            normalScale: new THREE.Vector2(0.15, 0.15),
            envMap: envmap.texture,
        };

        //Ball
        let ballGeo = new THREE.SphereGeometry(100, 64, 64);
        let ballMat = new THREE.MeshPhysicalMaterial(ballMaterial);
        let ballMesh = new THREE.Mesh(ballGeo, ballMat);
        scene.add(ballMesh);
    });
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);

    // const timer = clock.getElapsedTime(); //取得時間<由開始>
    controls.update();
    // _EffectComposer.render();
    renderer.render(scene, camera); //有effectComposer不可以雙重render !!!
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));
init();
animate();
