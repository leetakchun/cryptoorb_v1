import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { EffectComposer } from 'EffectComposer';
import { UnrealBloomPass } from 'UnrealBloomPass';
import { RenderPass } from 'RenderPass';
import { GLTFLoader } from 'GLTFLoader';
import { GlitchPass } from 'GlitchPass';

//setup global variable
let camera, scene, renderScene, renderer, container, controls, _EffectComposer, _UnrealBloomPass;
let geometry,
    material,
    mesh,
    cube,
    w,
    h,
    ratio,
    light1,
    light2,
    plane,
    clock,
    mixer,
    particles,
    velocities,
    particlesGeometry,
    particleMaterial,
    object3dMaterial,
    _instanceMesh,
    radiusVariance,
    _line,
    _lineMaterial;

let width = 2000;
let height = 2000;
let depth = 2000;
let speed = 8;
//Noise field zoom
let step = 2000;
//Camera rotate
let rotate = true;
//Offset to counteract noise flattening when sampling on three planes
let offset = 0.0;

const mobile =
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i);

let particleCount;
if (mobile) {
    particleCount = 200;
} else {
    particleCount = 2500;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(45, ratio, 0.1, 1000);
    camera.position.z = 45;
    camera.position.x = 3;
    camera.position.y = 20;

    //render
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(w, h); //may need w,h,false
    renderer.setPixelRatio(window.devicePixelRatio);
    // renderer.setClearColor(0x111b44); //background color
    container = document.getElementById('three_model'); //depends on div id
    container.appendChild(renderer.domElement);

    //EffectComposer > UnrealBloomPass
    _EffectComposer = new EffectComposer(renderer); //need webglrender for arg.
    _UnrealBloomPass = new UnrealBloomPass(new THREE.Vector2(w, h)); //constructor(resolution, strength, radius, threshold)

    _UnrealBloomPass.exposure = 0.3;
    _UnrealBloomPass.strength = 1.4;
    _UnrealBloomPass.radius = 1; //擴散到幾遠
    _UnrealBloomPass.threshold = 0.49; //演示程度

    _EffectComposer.setSize(w, h);
    _EffectComposer.addPass(new RenderPass(scene, camera));
    _EffectComposer.addPass(_UnrealBloomPass);
    // _EffectComposer.addPass(new UnrealBloomPass({ x: w, y: h }, 2.0, 0.0, 0.75));
    // let _Glitchpass = new GlitchPass(1);
    // _EffectComposer.addPass(_Glitchpass);

    //clock
    clock = new THREE.Clock();

    // SCENE
    scene.add(new THREE.AxesHelper(200));
    scene.background = new THREE.Color(0x000001);

    //Controls
    controls = new OrbitControls(camera, renderer.domElement); //centre of the div
    controls.update();

    // INIT HEMISPHERE LIGHT
    scene.add(new THREE.AmbientLight(0xffffff, 0.5));
    // POINT LIGHT
    light1 = new THREE.PointLight(0xff6666, 1, 100);
    light1.castShadow = true;
    light1.shadow.mapSize.width = 4096;
    light1.shadow.mapSize.height = 4096;
    scene.add(light1);
    light2 = new THREE.PointLight(0xff6666, 1, 100);
    light2.castShadow = true;
    light2.shadow.mapSize.width = 4096;
    light2.shadow.mapSize.height = 4096;
    scene.add(light2);

    //Spark
    radiusVariance = getRandomArbitrary(0.2, 1);
    // console.log(MESHLINE);

    //Particles
    particles = [];
    velocities = [];
    // object3dMaterial = new THREE.Object3D();
    particlesGeometry = new THREE.BufferGeometry();
    particleMaterial = new THREE.PointsMaterial({
        color: 0xff6800,
        size: 300,
        transparent: true,
        opacity: 1.0,
        //Other particles show through transparent sections of texture
        depthTest: false,
    });

    for (let i = 0; i < particleCount; i++) {
        let time = getRandomArbitrary(0, 100);
        let factor = getRandomArbitrary(20, 120);
        let speed = getRandomArbitrary(0.01, 0.015) / 2;
        let x = getRandomArbitrary(-50, 50);
        let y = getRandomArbitrary(-50, 50);
        let z = getRandomArbitrary(-50, 50);
        particles.push(x, y, z);
    }

    // let _dodecahedronGeometry = new THREE.DodecahedronGeometry(0.2, 0);
    // let _meshPhongMaterial = new THREE.MeshPhongMaterial();
    // _meshPhongMaterial.color = '#050505';
    // _instanceMesh = new THREE.InstancedMesh(_dodecahedronGeometry, _meshPhongMaterial);

    particlesGeometry.setAttribute('position', new THREE.Float32BufferAttribute(particles, 3));
    const points = new THREE.Points(particlesGeometry);
    // scene.add(points);

    //Geometry
    geometry = new THREE.BoxGeometry(10, 10, 10);
    material = new THREE.MeshStandardMaterial({ color: '#FFAE00', roughness: 0.8 });
    cube = new THREE.Mesh(geometry, material);
    cube.position.set(0, 4, 0);
    scene.add(cube);
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera); //有effectComposer不可以雙重render !!!
    // _EffectComposer.render();

    const now = Date.now() / 1000;
    light1.position.y = 40;
    light1.position.x = Math.cos(now) * 20;
    light1.position.z = Math.sin(now) * 20;
    light2.position.y = -20;
    light2.position.x = Math.sin(now) * 20;
    light2.position.z = Math.cos(now) * 20;

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));

init();
animate();
