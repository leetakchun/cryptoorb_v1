import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { EffectComposer } from 'EffectComposer';
import { UnrealBloomPass } from 'UnrealBloomPass';
import { RenderPass } from 'RenderPass';
import { GLTFLoader } from 'GLTFLoader';
import { GlitchPass } from 'GlitchPass';

//setup global variable
let camera, scene, renderScene, renderer, container, controls, _EffectComposer, _UnrealBloomPass;
let geometry, material, mesh, cube, w, h, ratio, light1, light2, plane, clock, mixer;

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(45, ratio, 0.1, 1000);
    camera.position.z = 45;
    camera.position.x = 3;
    camera.position.y = 20;

    //render
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(w, h); //may need w,h,false
    renderer.setPixelRatio(window.devicePixelRatio);
    // renderer.setClearColor(0x111b44); //background color
    container = document.getElementById('three_model'); //depends on div id
    container.appendChild(renderer.domElement);

    //EffectComposer > UnrealBloomPass
    _EffectComposer = new EffectComposer(renderer); //need webglrender for arg.
    _UnrealBloomPass = new UnrealBloomPass(new THREE.Vector2(w, h)); //constructor(resolution, strength, radius, threshold)
    _UnrealBloomPass.exposure = 0.3;
    _UnrealBloomPass.strength = 1.4;
    _UnrealBloomPass.radius = 1; //擴散到幾遠
    _UnrealBloomPass.threshold = 0.49; //演示程度
    let _Glitchpass = new GlitchPass(1);

    _EffectComposer.setSize(w, h);
    _EffectComposer.addPass(new RenderPass(scene, camera));
    _EffectComposer.addPass(_UnrealBloomPass);
    // _EffectComposer.addPass(_Glitchpass);
    // _EffectComposer.addPass(new UnrealBloomPass({ x: w, y: h }, 2.0, 0.0, 0.75));

    //clock
    clock = new THREE.Clock();

    // SCENE
    scene.background = new THREE.Color(0x000001);

    //Controls
    controls = new OrbitControls(camera, renderer.domElement); //centre of the div
    // controls.target = new THREE.Vector3(0, 0, -40); //set the target
    controls.update();

    // INIT HEMISPHERE LIGHT
    scene.add(new THREE.AmbientLight(0xffffff, 0.5));
    // POINT LIGHT
    light1 = new THREE.PointLight(0xff6666, 1, 100);
    light1.castShadow = true;
    light1.shadow.mapSize.width = 4096;
    light1.shadow.mapSize.height = 4096;
    scene.add(light1);
    light2 = new THREE.PointLight(0xff6666, 1, 100);
    light2.castShadow = true;
    light2.shadow.mapSize.width = 4096;
    light2.shadow.mapSize.height = 4096;
    scene.add(light2);

    // FLOOR
    // plane = new THREE.Mesh(new THREE.PlaneGeometry(200, 200), new THREE.MeshPhongMaterial({ color: 0x0a7d15 }));
    // plane.rotation.x = -Math.PI / 2;
    // plane.receiveShadow = true;
    // scene.add(plane);

    //Geometry
    geometry = new THREE.BoxGeometry(10, 10, 10);
    material = new THREE.MeshStandardMaterial({ color: '#FFAE00', roughness: 0.8 });
    cube = new THREE.Mesh(geometry, material);
    cube.position.set(0, 4, 0);
    scene.add(cube);
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    requestAnimationFrame(animate);

    // const timer = clock.getElapsedTime(); //取得時間<由開始>

    _EffectComposer.render();
    // renderer.render(scene, camera); //有effectComposer不可以雙重render !!!

    const now = Date.now() / 1000;
    light1.position.y = 40;
    light1.position.x = Math.cos(now) * 20;
    light1.position.z = Math.sin(now) * 20;
    light2.position.y = -20;
    light2.position.x = Math.sin(now) * 20;
    light2.position.z = Math.cos(now) * 20;

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));
init();
animate();
