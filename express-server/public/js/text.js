//setup global variable
let camera, scene, renderer, container, controls;
let geometry, material, mesh, cube, plane, light1, light2;

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(45, ratio, 0.1, 1000);
    camera.position.z = 45;
    camera.position.x = 3;
    camera.position.y = 20;
    //render
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(w, h); //may need w,h,false
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor(0x111b44);
    renderer.shadowMap.enabled = true;
    //Controls
    controls = new THREE.OrbitControls(camera, renderer.domElement); //centre of the div
    controls.target = new THREE.Vector3(0, 0, -40); //set the target
    controls.update();
    // SCENE
    scene.background = new THREE.Color(0xffffff);
    // FLOOR
    plane = new THREE.Mesh(new THREE.PlaneGeometry(200, 200), new THREE.MeshPhongMaterial({ color: 0x0a7d15 }));
    plane.rotation.x = -Math.PI / 2;
    plane.receiveShadow = true;
    scene.add(plane);
    // INIT HEMISPHERE LIGHT
    scene.add(new THREE.AmbientLight(0xffffff, 0.5));
    // POINT LIGHT
    light1 = new THREE.PointLight(0xff6666, 1, 100);
    light1.castShadow = true;
    light1.shadow.mapSize.width = 4096;
    light1.shadow.mapSize.height = 4096;
    scene.add(light1);

    light2 = new THREE.PointLight(0x33ff33, 1, 100);
    light2.castShadow = true;
    light2.shadow.mapSize.width = 4096;
    light2.shadow.mapSize.height = 4096;
    scene.add(light2);

    // TEXT LOADING
    const loader = new THREE.FontLoader();
    loader.load('../fonts/Faltura.json', function (font) {
        const geometry = new THREE.TextGeometry('1111 Go Go power ranger !\nSuper?!@#$', {
            font: font,
            size: 2,
            height: 1,
            curveSegments: 10,
            bevelEnabled: false,
            bevelOffset: 0,
            bevelSegments: 1,
            bevelSize: 0.3,
            bevelThickness: 1,
        });
        const materials = [
            new THREE.MeshPhongMaterial({ color: 0xff6600 }), // front
            new THREE.MeshPhongMaterial({ color: 0x0000ff }), // side
        ];
        const textMesh1 = new THREE.Mesh(geometry, materials);
        textMesh1.castShadow = true;
        textMesh1.position.y += 10;
        textMesh1.position.x -= 6;
        textMesh1.rotation.y = 0.25;
        scene.add(textMesh1);
    });

    //div
    container = document.getElementById('three_model'); //depends on div id
    container.appendChild(renderer.domElement);
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    const now = Date.now() / 1000;
    light1.position.y = 15;
    light1.position.x = Math.cos(now) * 20;
    light1.position.z = Math.sin(now) * 20;

    light2.position.y = 15;
    light2.position.x = Math.sin(now) * 20;
    light2.position.z = Math.cos(now) * 20;
    renderer.render(scene, camera);
    requestAnimationFrame(animate);
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));
init();
animate();
