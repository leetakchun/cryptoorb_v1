//setup global variable
let camera, scene, renderer, container;
let geomertry, material, mesh, cube;

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(70, ratio, 1, 200000);
    camera.position.z = 5;
    //render
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(w, h); //may need w,h,false
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor(0x111b44);
    container = document.getElementById('three_model'); //depends on div id
    container.appendChild(renderer.domElement);

    geometry = new THREE.BoxGeometry();
    material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function animate() {
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    renderer.render(scene, camera);
    requestAnimationFrame(animate);
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));
init();
animate();
