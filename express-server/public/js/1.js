import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { EffectComposer } from 'EffectComposer';
import { UnrealBloomPass } from 'UnrealBloomPass';
import { RenderPass } from 'RenderPass';
import { GLTFLoader } from 'GLTFLoader';
import { GlitchPass } from 'GlitchPass';
import { FontLoader } from 'FontLoader';
import { TextGeometry } from 'TextGeometry';
import { ShaderPass } from 'ShaderPass';

//setup global variable
let camera, scene, renderer, container, controls, _EffectComposer, _UnrealBloomPass;
let geometry, material, mesh, cube, light1, light2;
//bloom selective variable
let BLOOM_SCENE,
    bloomLayer,
    params,
    _materials,
    darkMaterial,
    bloomPass,
    renderPass,
    shaderPass,
    bloomComposer,
    finalComposer,
    glowingSphere,
    _sphere;
let TWO_PI = Math.PI * 2;
const mobile =
    navigator.userAgent.match(/Android/i) ||
    navigator.userAgent.match(/webOS/i) ||
    navigator.userAgent.match(/iPhone/i) ||
    navigator.userAgent.match(/iPod/i) ||
    navigator.userAgent.match(/BlackBerry/i) ||
    navigator.userAgent.match(/Windows Phone/i);
//Spatial variables
let width = 2000;
let height = 2000;
let depth = 2000;
let particleCount;
if (mobile) {
    particleCount = 200;
} else {
    particleCount = 1400;
}

let speed = 10;
//Noise field zoom
let step = 2000;
//Camera rotate
let rotate = false;
//Offset to counteract noise flattening when sampling on three planes
let offset = 0.0;

let distance = 400;
let FOV = (2 * Math.atan(window.innerHeight / (2 * distance)) * 90) / Math.PI;

let w;
let h;
let ratio;

let particles;
let velocities;
let colour;
let loader;

let texture;

function init() {
    scene = new THREE.Scene();
    w = window.innerWidth;
    h = window.innerHeight;
    ratio = w / h;
    camera = new THREE.PerspectiveCamera(FOV, ratio, 1, 200000);
    camera.position.set(-0.1 * width, -0.4 * height, -4.3 * width);
    // camera.position.z = 5000;
    // camera.position.x = 500;
    // camera.position.y = 500;
    scene.add(camera);

    //render
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(w, h);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor(0x111b44);
    container = document.getElementById('three_model');
    container.appendChild(renderer.domElement);

    //bloomlayer selective
    BLOOM_SCENE = 1;
    bloomLayer = new THREE.Layers();
    bloomLayer.set(BLOOM_SCENE);
    params = {
        exposure: 1,
        bloomStrength: 5, //control the effect mainly
        bloomThreshold: 0,
        bloomRadius: 0,
        scene: 'Scene with Glow',
    };
    darkMaterial = new THREE.MeshBasicMaterial({ color: 'black' });
    _materials = {};
    renderPass = new RenderPass(scene, camera);
    bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;
    //bloomComposer
    bloomComposer = new EffectComposer(renderer);
    bloomComposer.renderToScreen = false; //重點
    bloomComposer.addPass(renderPass);
    bloomComposer.addPass(bloomPass);
    //shaderpass
    shaderPass = new ShaderPass(
        new THREE.ShaderMaterial({
            uniforms: {
                baseTexture: { value: null },
                bloomTexture: { value: bloomComposer.renderTarget2.texture },
            },
            vertexShader: `
          varying vec2 vUv;
    
          void main() {
    
              vUv = uv;
    
              gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
          }`,
            fragmentShader: `
          uniform sampler2D baseTexture;
                uniform sampler2D bloomTexture;
    
                varying vec2 vUv;
    
                void main() {
    
                    gl_FragColor = ( texture2D( baseTexture, vUv ) + vec4( 1.0 ) * texture2D( bloomTexture, vUv ) );
    
                }
          `,
            defines: {},
        }),
        'baseTexture',
    );
    shaderPass.needsSwap = true;
    //finalComposer
    finalComposer = new EffectComposer(renderer);
    finalComposer.addPass(renderPass);
    finalComposer.addPass(shaderPass);

    //找出所有的object並移除所有material,  in the scene
    scene.traverse((obj) => {
        if (obj.material) {
            obj.material.dispose();
        }
    });

    // scene.children.length = 0;
    const sphereGeometry = new THREE.IcosahedronBufferGeometry(200, 4);
    const sphereMaterial = new THREE.MeshPhongMaterial({
        // color: new THREE.Color().setHSL(Math.random(), 0.7, Math.random() * 0.2 + 0.05),
        color: 'green',
    });
    glowingSphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
    glowingSphere.position.set(0, 0, 1000);
    // scene.add(glowingSphere);
    // glowingSphere.layers.enable(BLOOM_SCENE); //generate mark
    // console.log(glowingSphere);

    // _sphere = createSphere();
    // _sphere.position.set(-500, 1000, 0);
    // scene.add(_sphere);

    //Composer
    _EffectComposer = new EffectComposer(renderer); //need webglrender for arg.
    _UnrealBloomPass = new UnrealBloomPass(new THREE.Vector2(w, h)); //constructor(resolution, strength, radius, threshold)
    _UnrealBloomPass.exposure = 0.3;
    _UnrealBloomPass.strength = 1;
    _UnrealBloomPass.radius = 1.2; //擴散到幾遠
    _UnrealBloomPass.threshold = 0.6; //演示程度
    let _Glitchpass = new GlitchPass(1);

    _EffectComposer.setSize(w, h);
    _EffectComposer.addPass(new RenderPass(scene, camera));
    _EffectComposer.addPass(_UnrealBloomPass);
    // _EffectComposer.addPass(_Glitchpass);
    // _EffectComposer.addPass(new UnrealBloomPass({ x: w, y: h }, 2.0, 0.0, 0.75));

    //https://discourse.threejs.org/t/is-there-a-site-to-load-three-library-no-hotlinking/6065
    //OrbitControls.js for camera manipulation
    controls = new OrbitControls(camera, renderer.domElement);
    controls.maxDistance = 200000 - 5 * width;
    controls.minDistance = 100;
    controls.autoRotate = rotate;
    controls.autoRotateSpeed = 2;

    //Particles
    particles = [];
    velocities = [];
    geometry = new THREE.BufferGeometry();
    //Initial ember colour
    colour = 0xff6800;

    //Add texture to particles
    loader = new THREE.TextureLoader();
    //https://stackoverflow.com/questions/24087757/three-js-and-loading-a-cross-domain-image
    THREE.ImageUtils.crossOrigin = '';
    texture = loader.load('https://al-ro.github.io/images/embers/ember_texture.png');
    //Variable size for particle material
    let size = 200;
    if (mobile) {
        size = 250;
    }

    //material >> Particle
    material = new THREE.PointsMaterial({
        color: 0xff6800,
        size: size,
        transparent: true,
        opacity: 1.0,
        map: texture,
        //Other particles show through transparent sections of texture
        depthTest: false,
    });
    //For glow effect

    //https://threejs.org/docs/#api/en/constants/Materials 似photoshop 的 混合模式
    material.blending = THREE.AdditiveBlending; //default

    //Generate random particles
    for (let i = 0; i < particleCount; i++) {
        let x = width / 2 - Math.random() * width;
        let y = height / 2 - Math.random() * height;
        let z = depth / 2 - Math.random() * depth;
        let vel_x = 0.5 - Math.random();
        let vel_y = 0.5 - Math.random();
        let vel_z = 0.5 - Math.random();

        particles.push(x, y, z);
        velocities.push(vel_x, vel_y, vel_z);
    }
    geometry.setAttribute('position', new THREE.Float32BufferAttribute(particles, 3));
    const points = new THREE.Points(geometry, material);
    scene.add(points);

    // INIT HEMISPHERE LIGHT
    scene.add(new THREE.AmbientLight(0xffffff, 2));
    // POINT LIGHT
    light1 = new THREE.PointLight(0xff6666, 1, 100);
    // scene.add(light1);

    light2 = new THREE.PointLight(0x33ff33, 1, 100);
    // scene.add(light2);

    // TEXT LOADING
    const textLoader = new FontLoader();
    textLoader.load('../fonts/Faltura.json', function (font) {
        const textGeometry = new TextGeometry('W e l o c m e !\n0 x 1 2 3 4 5 6 7 8 9', {
            font: font,
            size: 600,
            height: 100,
            curveSegments: 10,
            bevelEnabled: false,
            bevelOffset: 0,
            bevelSegments: 1,
            bevelSize: 0.3,
            bevelThickness: 1,
        });
        const textGeometry2 = new TextGeometry('W e l o c m e !\n0x123456789', {
            font: font,
            size: 550,
            height: 100,
            curveSegments: 10,
            bevelEnabled: false,
            bevelOffset: 0,
            bevelSegments: 1,
            bevelSize: 0.3,
            bevelThickness: 1,
        });
        //get text center location
        textGeometry.computeBoundingBox(); //need to compute
        let { x, y, z } = textGeometry.boundingBox.max;
        x = x / 2; //calculate Center
        y = y / 2; //calculate Center
        z = z / 2; //calculate Center

        textGeometry2.computeBoundingBox(); //need to compute
        let { x2, y2, z2 } = textGeometry2.boundingBox.max;
        x2 = x2 / 2; //calculate Center
        y2 = y2 / 2; //calculate Center
        z2 = z2 / 2; //calculate Center

        const materials = [
            new THREE.MeshPhongMaterial({ color: 0xff6600 }), // front
            new THREE.MeshPhongMaterial({ color: 0x0000ff }), // side
        ];
        const textMesh1 = new THREE.Mesh(textGeometry, materials);
        textMesh1.castShadow = true;
        textMesh1.position.y = y * 0.5;
        textMesh1.position.x = x;
        textMesh1.position.z = z;
        textMesh1.rotation.y = THREE.Math.degToRad(180);
        scene.add(textMesh1);

        // const materials2 = [
        //     new THREE.MeshBasicMaterial({ color: 0xff6600 }), // front
        //     new THREE.MeshBasicMaterial({ color: 0x0000ff }), // side
        // ];
        const textMesh2 = new THREE.Mesh(textGeometry, darkMaterial);
        textMesh2.castShadow = true;
        textMesh2.position.y = y * 0.5;
        textMesh2.position.x = x;
        textMesh2.position.z = z * -10;
        textMesh2.rotation.y = THREE.Math.degToRad(180);
        // scene.add(textMesh2);
    });
}

function createSphere() {
    const geometry = new THREE.IcosahedronBufferGeometry(500, 4);
    const material = new THREE.MeshPhongMaterial({
        color: new THREE.Color().setHSL(Math.random(), 0.7, Math.random() * 0.2 + 0.05),
    });
    const sphere = new THREE.Mesh(geometry, material);
    return sphere;
}

//----------NOISE---------//
//Use noise.js library to generate a grid of 3D simplex noise values
const simplex = new SimplexNoise();

function computeCurl(x, y, z) {
    // var eps = 0.0001;
    var eps = 0.000001;

    var curl = new THREE.Vector3();

    //Find rate of change in YZ plane
    var n1 = simplex.noise2D(x, y + eps, z);
    var n2 = simplex.noise2D(x, y - eps, z);
    //Average to find approximate derivative
    var a = (n1 - n2) / (2 * eps);
    var n1 = simplex.noise2D(x, y, z + eps);
    var n2 = simplex.noise2D(x, y, z - eps);
    //Average to find approximate derivative
    var b = (n1 - n2) / (2 * eps);
    curl.x = a - b;

    //Find rate of change in XZ plane
    n1 = simplex.noise2D(x, y, z + eps);
    n2 = simplex.noise2D(x, y, z - eps);
    a = (n1 - n2) / (2 * eps);
    n1 = simplex.noise2D(x + eps, y, z);
    n2 = simplex.noise2D(x - eps, y, z);
    b = (n1 - n2) / (2 * eps);
    curl.y = a - b;

    //Find rate of change in XY plane
    n1 = simplex.noise2D(x + eps, y, z);
    n2 = simplex.noise2D(x - eps, y, z);
    a = (n1 - n2) / (2 * eps);
    n1 = simplex.noise2D(x, y + eps, z);
    n2 = simplex.noise2D(x, y - eps, z);
    b = (n1 - n2) / (2 * eps);
    curl.z = a - b;

    return curl;
}

function move(particles) {
    for (let i = 0; i < particleCount * 3.0; i += 3) {
        //Find curl value at partile location
        var curl = computeCurl(particles[i] / step, particles[i + 1] / step, particles[i + 2] / step);

        //Update particle velocity according to curl value and speed
        velocities[i] = speed * curl.x;
        velocities[i + 1] = speed * curl.y;
        velocities[i + 2] = speed * curl.z;

        //Update particle position based on velocity
        particles[i] += velocities[i];
        particles[i + 1] += velocities[i + 1];
        particles[i + 2] += velocities[i + 2];

        //Boudnary conditions
        //If a particle gets too far away from (0,0,0), reset it to a random location
        var dist = Math.sqrt(particles[i] * particles[i] + particles[i + 1] * particles[i + 1] + particles[i + 2] * particles[i + 2]);
        //set from dist > 5 * width to dist > 2 * width 避免走出太遠
        if (dist > 2 * width) {
            particles[i] = width / 2 - Math.random() * width;
            particles[i + 1] = height / 2 - Math.random() * height;
            particles[i + 2] = depth / 2 - Math.random() * depth;
        }
    }

    geometry.getAttribute('position').copyArray(particles);
    geometry.getAttribute('position').needsUpdate = true;
}

function onWindowResize() {
    //updating
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

//Bloom Selective function
function disposeMaterial(obj) {
    if (obj.material) {
        obj.material.dispose();
    }
}

function darkenNonBloomed(obj) {
    if (obj.isMesh && bloomLayer.test(obj.layers) === false) {
        _materials[obj.uuid] = obj.material;
        obj.material = darkMaterial;
    }
}
function restoreMaterial(obj) {
    if (_materials[obj.uuid]) {
        // console.log('obj >> ', obj);
        // console.log('obj.uuid >>', obj.uuid);
        // console.log('_materials > ', _materials);
        obj.material = _materials[obj.uuid];
        delete _materials[obj.uuid];
    }
}

function animate() {
    if (rotate) {
        controls.update();
    }

    // scene.traverse(darkenNonBloomed);
    // bloomComposer.render();
    // scene.traverse(restoreMaterial);
    // finalComposer.render();

    // renderer.render(scene, camera); //有effectComposer不可以雙重render !!!
    _EffectComposer.render();
    move(particles);
    requestAnimationFrame(animate);
}

function debounce(callback, time) {
    let _time = time || 200; //default 200ms
    let timer;
    return function (e) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(callback, _time, e);
    };
}

window.addEventListener('resize', debounce(onWindowResize, 300));
init();
animate();
