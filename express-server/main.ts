import express from 'express';
import expressSession from "express-session";
import dotenv from "dotenv";
import socketIO from "socket.io";
import cors from "cors";
import http from "http";
import path from "path";
import { setSocketIO } from "./socketio";
// import {Request,Response} from 'express';
import { createRouter } from "./routers";

//server setting
dotenv.config();
let app = express();
let server = http.createServer(app);
let io = new socketIO.Server(server);
setSocketIO(io);

//set cors and handle json
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//express session
let sessionMiddleware = expressSession({
    secret: "Memo Wall will not down",
    saveUninitialized: true,
    resave: true,
  });
app.use(sessionMiddleware);

app.use("/",express.static("public"));

//set router
let router = createRouter();
app.use(router);

//handle 404
app.use((req, res) => {
    res.sendFile(path.join(__dirname, "public", "404.html"));
  });

//start server
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});