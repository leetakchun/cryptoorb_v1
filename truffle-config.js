require('dotenv').config();
const path = require('path');
const HDWalletProvider = require('@truffle/hdwallet-provider');
const RINKEBY_ACCOUNT_PRIVATE_KEY = process.env.RINKEBY_ACCOUNT_PRIVATE_KEY;
const RINKEBY_PRC_URL = process.env.RINKEBY_PRC_URL;
const ETHERSCAN_API_KEY = process.env.ETHERSCAN_API_KEY;

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    contracts_build_directory: path.join(__dirname, 'client/src/contracts'),
    networks: {
        develop: {
            port: 7545,
        },
        rinkeby: {
            provider: () =>
                new HDWalletProvider({
                    privateKeys: [`${RINKEBY_ACCOUNT_PRIVATE_KEY}`],
                    providerOrUrl: RINKEBY_PRC_URL,
                    numberOfAddresses: 1,
                }),
            network_id: '4',
            gas: 5500000,
            networkCheckTimeout: 1000000,
            addressIndex: 0,
            confirmation: 2,
            timeoutBlocks: 200,
            skipDryRun: true,
        },
    },
    compilers: {
        solc: {
            version: '0.8.1',
            optimizer: {
                enable: true,
                runs: 200,
            },
        },
    },
    etherscan: {
        apiKey: ETHERSCAN_API_KEY,
    },
};
