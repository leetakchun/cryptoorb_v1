//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
// ../client/node_modules/
import "../client/node_modules/@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../client/node_modules/@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol"; //totalSupply()
import "../client/node_modules/@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol"; //setTokenURI()
import "../client/node_modules/base64-sol/base64.sol";
import "../client/node_modules/@openzeppelin/contracts/access/Ownable.sol"; //onlyOwner <contract creator>
import "../client/node_modules/@openzeppelin/contracts/finance/PaymentSplitter.sol";
//DAO voting
import "../client/node_modules/@openzeppelin/contracts/token/ERC721/extensions/draft-ERC721Votes.sol";

//library
import "../client/node_modules/@openzeppelin/contracts/utils/Counters.sol";
import "../client/node_modules/@openzeppelin/contracts/utils/Address.sol";
import "../client/node_modules/@openzeppelin/contracts/utils/Strings.sol";
import "../client/node_modules/@openzeppelin/contracts/utils/math/Math.sol";
import "../client/node_modules/@openzeppelin/contracts/utils/Context.sol";

import "../client/node_modules/@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract CryptoOrb is ERC721, ERC721Enumerable, ERC721URIStorage, Ownable, ReentrancyGuard{

    using Address for address payable;
    using Counters for Counters.Counter; 
    using Strings for uint256; 
    using Strings for uint8;

    Counters.Counter private _tokenId; 
    uint256 public cost = 0.1 ether; //ether is specific value
    uint256 public maxSupply = 10000;
    uint256 public maxMintAmount = 20;
    bool public paused = false; //control of the smart contract

    constructor() ERC721("CryptoOrb","COB"){
    }

    //generate random number 0 to 999
    function random() private view onlyOwner returns (uint)  {
        uint randomHash = uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp)));
        return randomHash % 1000;           
    } 

    function mint(
        address _to,
        uint256 _mintAmount,
        string memory _name,
        string memory _material,
        string memory _color,
        string memory _animationURL,
        string memory _imageURL,
        string memory _description,
        uint8 _level
        ) external payable nonReentrant{ //nonReentrant @ReentrancyGuard.sol
            uint256 supply = totalSupply(); 
            require(!paused);
            require(_mintAmount > 0);
            require(supply + _mintAmount <= maxSupply); //for all user
            require(_to == msg.sender); // front end redux store checking

            //Limitation for non-Contract-Creator
            if (msg.sender != owner()) { //owner() get contract-creator
                    uint256 ownerTokenCount = balanceOf(msg.sender); //@ERC721 check user
                    // console.log("ownerTokenCount > ", ownerTokenCount);
                    // console.log("ownerTokenCount + _mintAmount > ", ownerTokenCount + _mintAmount);
                    require((ownerTokenCount + _mintAmount) <= maxMintAmount);//for all user
                    require(msg.value >= cost * _mintAmount);   
            }

            for(uint256 i =1; i<= _mintAmount; i++){
                _safeMint(msg.sender, _tokenId.current());
                string memory _currentTokenURI = formatTokenURI(_name, _animationURL,  _imageURL, _description, _material, _color, _level);
                // console.log("_currentTokenURI >>", _currentTokenURI);
                _setTokenURI(_tokenId.current(), _currentTokenURI);
                _tokenId.increment(); //update token id
            }     
    }

      function walletOfOwner(address _owner)
            public
            view
            returns (uint256[] memory)
        {
            uint256 ownerTokenCount = balanceOf(_owner);
            uint256[] memory tokenIds = new uint256[](ownerTokenCount); //create array
            for (uint256 i; i < ownerTokenCount; i++) {
                tokenIds[i] = tokenOfOwnerByIndex(_owner, i); //@ERC721Enumerale.sol ==> 2D array
                //_ownedTokens = [_owner1:[1:nft1, 2:nft2, 3:nft3 ...], _owner2:[1:nft4, 2:nft5, 3:nft6...] ]
                //_ownedTokens[_owner1] ==> _owner1 = [1:nft1, 2:nft2, 3:nft3, 4:nft4 ...]
                //_owner[0] = nft1

                // mapping(uint256 => uint256) private _ownedTokensIndex; @ERC721Enumerale.sol
                // _ownedTokensIndex = [nft1 : 1, nft2: 2, nft3: 3, nft4: 1, nft5:2  ...] 
                // <nft1,nft2, nft3 own by _owner1> <nft4, nft5 own by _owner2>
            }
            return tokenIds; //return array for NFT ids e.g. tokenIds = [1,2,3]
        }


    function formatTokenURI(string memory _name, 
                            string memory _animationURL,
                            string memory _imageURL,
                            string memory _description,
                            string memory _material, 
                            string memory _color, 
                            uint8 _level
                            )public pure returns (string memory){
        string memory json = Base64.encode(
            bytes(string(
                abi.encodePacked(combinePartA(_name,_animationURL,_imageURL,_description),combinePartB(_material,_color,_level))
            ))
        );
        return string(abi.encodePacked('data:application/json;base64,', json));
    }

//////// prevent Stack Too Deep (max 16 variable) ////////
    function combinePartA(string memory _name, 
                            string memory _animationURL,
                            string memory _imageURL,
                            string memory _description) public pure returns(string memory){
                               return string(abi.encodePacked(
                                    '{"name": "', _name, '",',
                                    '"image": "', _imageURL, '",',
                                    '"animation_url": "', _animationURL, '",',
                                    '"description": "', _description, '",'
                                ));
                            }

    function combinePartB(  string memory _material, 
                            string memory _color, 
                            uint8 _level) public pure returns(string memory){
                               return string(abi.encodePacked(
                                    '"attributes": [{"trait_type": "Color", "value": "', _color, '"},',
                                    '{"trait_type": "Level", "value": ', _level.toString(), '},',
                                    '{"trait_type": "Material", "value": "', _material, '"}',
                                    ']}'
                                ));
                            }
                    


////////// onlyOwner Function ////////////
    function getAllTokenId() public view onlyOwner returns (uint256[] memory){
        uint256 _totalSupply =  ERC721Enumerable.totalSupply();
        uint256[] memory getAllTokenIds = new uint256[](_totalSupply); //create new array
        // console.log("_totalSupply >>", _totalSupply);
        for(uint256 i = 0; i< _totalSupply; i++){
            getAllTokenIds[i] = ERC721Enumerable.tokenByIndex(i);
            // console.log("ERC721Enumerable.tokenByIndex(i) > >", getAllTokenIds[i]);
        }
        return getAllTokenIds;
    }

    function setCost(uint256 _newCost) public onlyOwner {   
        // console.log("_newCost > ", _newCost);
        //change to ether unit
        cost = _newCost; // TODO resume only 
        // cost = _newCost * 1e18; // for testing only 
    }

    function setmaxMintAmount(uint256 _newmaxMintAmount) public onlyOwner {
        maxMintAmount = _newmaxMintAmount;
    }

    function pause(bool _state) public onlyOwner {
        paused = _state;
    }
    
    // public payable function, withdraw fund from contact
    function withdraw() public payable onlyOwner {
        require(payable(msg.sender).send(address(this).balance)); //send ETH to msg.sender from address(this)
    }

    /////// For off-chain metadata ///////
    // function setBaseURI(string memory _newBaseURI) public onlyOwner{
    //     baseURI = _newBaseURI;
    // }
    // function setBaseExtension(string memory _newBaseExtension) public onlyOwner {
    //     baseExtension = _newBaseExtension;
    // }


/////////OVERRIDE/////////
/////////Override since among ERC721, ERC721Enumerable, ERC721URIStorage have the same function///////////
    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}